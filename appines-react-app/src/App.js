import logo from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p className="App-title">
          Bienvenue sur la plateforme App'Ines
        </p>
        <a
          className="App-link"
          href="https://www.appines.fr/"
          target="_blank"
          rel="noopener noreferrer"
        >
          En savoir plus
        </a>
      </header>
    </div>
  );
}

export default App;
