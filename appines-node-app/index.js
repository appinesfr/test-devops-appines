const express = require('express')
const bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient
const app = express()
require('dotenv').config();
const ObjectId = require('mongodb').ObjectId


MongoClient.connect(process.env.MONGO_URL, { useUnifiedTopology: true })
    .then(client => {
        console.log('Connected to Database');
        const db = client.db(process.env.DB_NAME);
        const usersCollection = db.collection('users');

        // ========================
        // Middlewares
        // ========================
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());

        // ========================
        // Routes
        // ========================
        app.get('/', (req, res) => {
            res.json('Welcome to appInes');
        });

        app.get('/users', (req, res) => {
            usersCollection.find().toArray()
                .then(users => {res.json(users)})
                .catch(error => res.status(400).send({error: error}));
        });

        app.post('/users', (req, res) => {
            if (req.body.name) {
                usersCollection.insertOne(req.body)
                    .then(result => {res.json(result)})
                    .catch(error => res.status(400).send({error: error}));
            } else
                res.status(400).send({message: 'No name in body'});
        });

        app.put('/users', (req, res) => {
            if (req.body._id && req.body.name) {
                usersCollection.findOneAndUpdate(
                    { _id: ObjectId(req.body._id) },
                    {
                        $set: {
                            name: req.body.name,
                        }
                    }).then(result => res.json(result))
                    .catch(error => res.status(400).send({error: error}))
            } else
                res.status(400).send({message: 'No name or _id in body'});
        })

        app.delete('/users', (req, res) => {
            if (req.body._id) {
                usersCollection.deleteOne(
                    {_id: ObjectId(req.body._id)}
                )
                    .then(result => {
                        if (result.deletedCount === 0) {
                            return res.status(400).send({error: '0 user deleted'});
                        }
                        res.json(result);
                    })
                    .catch(error => res.status(400).send({error: error}))
            } else
                res.status(400).send({message: 'No _id in body'});
        })

        // ========================
        // Listen
        // ========================
        const isProduction = process.env.NODE_ENV === 'production';
        const port = isProduction ? 4444 : 5555;
        app.listen(port, function () {
            console.log(`Listening on port : ${port}`);
        });

    })
    .catch(console.error)