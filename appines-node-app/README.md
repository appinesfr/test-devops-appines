# Appines Node App


A simple NodeJS API with MongoDB.

## Install

1. Install [MongoDB](https://www.mongodb.com/)
2. Install [Node.js](https://nodejs.org/en/)
3. Install project :

```
npm install
```

## ENVIRONMENT VARIABLE

This package provides a flexible, customizable, and low-dependency Representational State Transfer (REST) Application Programming Interface (API) for [mongodb](https://www.npmjs.com/package/mongodb) using [express](https://www.npmjs.com/package/express).

It is recommended to use a `.env` file at the root of your project directory with the following contents:

* `MONGO_URL`: MongoDB [connection string](https://docs.mongodb.com/manual/reference/connection-string/)
* `DB_NAME`: MongoDB database name

```
MONGO_URL=mongodb://localhost:27017
DB_NAME=appines-test
```

The `.env` file above can be loaded using [dotenv](https://www.npmjs.com/package/dotenv):

```javascript
require('dotenv').config();
```

See [Documentation](https://rrwen.github.io/express-mongodb-rest) for more details.

## USAGE

Run the file `index.js` defined above:

```
node index.js
```

Go to `https://localhost:5555/` to use the API.


## ROUTES


Method | Route | Function | Body | Description
--- | --- | --- | --- | ---
GET | /users | [find](https://mongodb.github.io/node-mongodb-native/3.0/api/Collection#find) | {} | Find all documents in collection
POST | /users| [insertOne](https://mongodb.github.io/node-mongodb-native/3.0/api/Collection.html#insertOne)| {name: "value"}| Insert `{name: "value"}` into `users` collection
PUT | /users | [findOneAndUpdate](https://mongodb.github.io/node-mongodb-native/3.0/api/Collection.html#findOneAndUpdate) | {_id: "objectId", name: "newValue"} | Update `{_id: "objectId"}` with `{name: "newValue"}`
DELETE | /users | [deleteOne](https://mongodb.github.io/node-mongodb-native/3.0/api/FindOperatorsOrdered.html#deleteOne) | {_id: "objectId"} | Delete the _id document
