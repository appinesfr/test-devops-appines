# DEVOPS TECHNICAL TEST - APP'INES

## Introduction

The company you work for has a backend made in NodeJS connected to a no-sql database (MongoDB). Their front-end is a simple React app.

You will find these two parts in the appropriate folders. In each folder is a README which explains how the applications work.

##  INSTRUCTIONS

You must dockerize these applications.
Depending on the environment, some configuration will be different.
In order to ensure the good follow-up of the projects you must realize simple documentation so that the future developers can easily install their working environment.
